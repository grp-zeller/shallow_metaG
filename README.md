# shallow_metaG

This repository contains the scripts for the Shallow metagenomic metaanalysis. 
Shallow shotgun metagenomic sequencing is thought to be a viable alternative to deep shotgun metagenomic
sequencing and 16S-RNA profiling, combining the benefits of high taxonomic resolution
and low costs. When compared to deep sequencing, shallow sequencing comes inevitably with a loss of information, which
might impact downstream statistical analyses and machine learning processes. 
To identify the exact consequences on downstream applications, we downsampled mOTUs 
profiles generated from over 20 datasets to different count levels and evaluated 
the information content sustained in the data throughout the downsampling process.
We also trained LASSO regression models on downsampled data, evaluate their 
predictive power and tested their transferability on other shallow and deep data.


#### Requirements

The analysis was performed in R version 4.0.0. A list of packages and their versions
is added soon.

#### Download and prepare data

The datasets analyzed here are available on Zenodo. To download the data, run:
```bash
Rscript ./download_data.R
```

the Japanese dataset still has to be included in the Zenodo repository. To bring
the japanese dataset in the right format and perform minor cleanups, run:
```bash
Rscript ./fix_JP_file.R
```

In the metadata of some studies, the initially annotated read counts are not 
properly calculated. To calculate the actual library sizes from annotation output
files (origin: motus profiler if I remember correctly), run:
```bash
Rscript ./fqstats_to_readcounts.R
```

Before running the next script, consider to which number of mOTU counts you want
to downsample your data to. The default is set to 10k, 5k, 2k, 1k, 500, 200 and 100.
Some studies contain more than one type of disease in the data, for example cases of
Crohn's disease (CD) and ulcerative colitis (UC). To make the data immediately
processable for the SIAMCAT package, data of two different diseases in a singe
dataset are split into separate data structures. Some studies also contain unwanted
subpopulations like certain disease types or control populations which differ
geographically from the case populations. Lastly, the script also generates 
auxiliar.tsv containing general information about the datasets and their metrics.
To clean the data and generate the auxiliar.tsv, run:
```bash
Rscript ./dataset_info.R
```
#### downsample data

To generate an atequate representation of shallow sequcencing data from deep 
sequencing data, run: 
```bash 
Rscript ./downsampling.R
```

For every study in the dataset, a single .Rdata object is generated containing
ten downsampling instances of each downsampling level provided in dataset_info.R 
The probability density function (even though a probability mass function would make more sense, right?)
of mOTU count sums across all samples in a dataset is assumed to follow 
a normal distribution. From the mOTU count sums, the mean, standard deviation (sd) and mean-sd 
ratio is calculated. The mean of the normal distribution representing the new 
downsampled data is set to the downsampling count level. The downsampling sd is calculated
using the new mean and the original distribution mean-sd ratio. For each sample, 
a value is drawn from the new probability density function to recieve a downsampled sum.
For each sample, the downsampling process distributes counts totalling the newly drawn sum of mOTU
counts across the mOTUs while also imitating the mOTU count distribution of the original 
sample. Since this process is performed on count data, mOTU with low counts in the
original data might disappear due to townsampling. 

#### generate script infrastructure to parallelize downstream applications

This analysis was performed on a high performance computing cluster using the
job scheduler slurm. Parallelizing jobs to reduce consecutive run time was done 
using an array of slurm scripts which can be passed to the scheduler at once.

To generate the slurm scripts, run:
```bash
Rscript ./generate_slurm_scripts.R
```

#### train SIAMCAT models

Machine learning models are trained on the datasets using the SIAMCAT package. 
The count data is transformed into differential abundances, removing any mOTU 
with no relative abundances higher than 1e-08 in any sample of the dataset. 
The associations between features (mOTUs) and label (healthy/diseased) are calculated
and the features are transformed into a logarithmic scale and z-score standardized.
Ten resampling rounds of the data are used to perform tenfold cross validation/
are used to ten fold cross validate the data/ are used to do ten cross validations
(not sure how the language works here)
In total, 100 lasso models are trained per SIAMCAT object. The models are used to
predict the outcomes of the left out cross validation data segment and the 
prediction quality is evaluated. 

Key features of the SIAMCAT objects, like the original feature matrix, the results 
of the association testing as well as the average area under the reciever 
operating curve (AUROC) are saved in external files.

To train SIAMCAT models on your datasets, enter the following in the command line:
```bash
for i in /slurm_scripts/model_training/*.sh
do
sbatch $i
done
```

#### test SIAMCAT models on other datasets

The transferability of machine learning models is tested by predicting the label 
information of an unknown dataset using a model previously trained on the initial
dataset. The quality of the transfer is assessed through the true positive rate 
and false positive rate of the predictions with a threshold found in the original 
model at a 10% false positive rate.

To evaluate the transferability of the SIAMCAT models, run:
```bash
for i in /slurm_scripts/model_testing/*.sh
do
sbatch $i
done
```

#### Correlation of species abundances

<!---
To identify critical thresholds of sequencing depth for key 
predictor species
-->


Low abundant species become more unlikey to detect the shallower the sequencing 
data becomes. The overall representation quality of the gut microbiome
through a shallow-count sample is difficult to estimate when observing it in
isolation. By comparing a shallow sample with itself at deeper mOTU count levels, it is possible
to estimate the loss of information the sample suffers when analysed in its more
shallow states. The script correlation_species_abundances.R calculates different 
correlation metrices between a sample in its original, deepest sequencing state 
and its more shallow variations. Declining correlation represents a loss of information
caused by the downsampling process.
Different types of correlations are calculated for each shallow sample and its 
original depth counterpart, namely Spearman Correlation, log(Pearson) Correlation
and Jaccard Similarity, as well as general population metrices such as Shannon Diversity
and Species Richness. 

To calculate the correlations between samples and the population metrices, run:
```bash
for i in /slurm_scripts/correlation_species_abundances/*.sh
do
sbatch $i
done
```

#### Blocked Wilcoxon testing

To calculate the significance of changes in relative abundance across studies,
the fact that data originates from different studies has to be taken into account.
Blocked testing in this case is used to control for variability introduced by the factor
study that could otherwise not be explained. To identify differential abundant 
species across studies, a blocked Wilcoxon test is applied on the original abundance
data. Combining studies in this way greatly enhances the statistical power of the analysis
due to increased number of samples. Identifying differentially abundant species
in the blocked wilcoxon test applied on deep sequencing data establishes these
species as reliable marker species across studies. These species can be specifically 
looked after when analyzing shallow data to evaluate the potency of shallow sequencing.

To run the blocked Wilcoxon tests, run:
```bash
for i in /slurm_scripts/Blocked_Wilcoxon/*.sh
do
sbatch $i
done
```

#### Summarise output data and reproduce Figures

Parallelized calculations result in arrays of output files which have to be 
summarized into a single file. To join output data produced from parallelized jobs, 
run:
```bash
Rscript ./summarise_data.R
```

Based on this joint output data, graphs identical to those found in the report 
are generated by the FigureX.R scripts. The plots will be saved in the plots 
folder where the scripts each generate a separate folder for their plots, labeled
according to the Figure they contribute to. To generate all plots, run:
```bash
Rscript ./Figure1.R
Rscript ./Figure2.R
Rscript ./Figure3.R
Rscript ./Figure4a.R
Rscript ./Figure4b.R
