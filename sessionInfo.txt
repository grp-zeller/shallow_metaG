R version 4.0.0 (2020-04-24)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /g/easybuild/x86_64/CentOS/7/haswell/software/OpenBLAS/0.3.9-GCC-9.3.0/lib/libopenblas_haswellp-r0.3.9.so

locale:
 [1] LC_CTYPE=C                 LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] parallel  tools     stats     graphics  grDevices utils     datasets 
[8] methods   base     

other attached packages:
 [1] data.table_1.13.0  matrixStats_0.56.0 ggrepel_0.8.2      cowplot_1.0.0     
 [5] rstatix_0.5.0      ggpubr_0.3.0       vegan_2.5-6        lattice_0.20-41   
 [9] permute_0.9-5      Matrix_1.2-18      staRdom_1.1.14     eemR_1.0.1        
[13] coin_1.3-1         survival_3.1-12    optparse_1.6.6     scales_1.1.0      
[17] SIAMCAT_1.8.1      phyloseq_1.32.0    mlr_2.17.1         ParamHelpers_1.14 
[21] here_0.1           forcats_0.5.0      stringr_1.4.0      dplyr_1.0.2       
[25] purrr_0.3.4        readr_1.3.1        tidyr_1.1.2        tibble_3.0.4      
[29] ggplot2_3.3.2      tidyverse_1.3.0   

loaded via a namespace (and not attached):
  [1] readxl_1.3.1        backports_1.1.6     fastmatch_1.1-0    
  [4] drc_3.0-1           corrplot_0.84       cdom_0.1.0         
  [7] plyr_1.8.6          igraph_1.2.5        splines_4.0.0      
 [10] multiway_1.0-6      gridBase_0.4-7      TH.data_1.0-10     
 [13] foreach_1.5.0       viridis_0.5.1       fansi_0.4.1        
 [16] magrittr_1.5        checkmate_2.0.0     BBmisc_1.11        
 [19] cluster_2.1.0       doParallel_1.0.15   openxlsx_4.1.4     
 [22] Biostrings_2.56.0   modelr_0.1.6        R.utils_2.9.2      
 [25] sandwich_2.5-1      prettyunits_1.1.1   colorspace_1.4-1   
 [28] rvest_0.3.5         haven_2.2.0         crayon_1.3.4       
 [31] jsonlite_1.6.1      libcoin_1.0-5       R.matlab_3.6.2     
 [34] zoo_1.8-8           iterators_1.0.12    ape_5.3            
 [37] glue_1.4.0          gtable_0.3.0        zlibbioc_1.34.0    
 [40] XVector_0.28.0      car_3.0-7           Rhdf5lib_1.10.1    
 [43] shape_1.4.4         BiocGenerics_0.34.0 abind_1.4-5        
 [46] mvtnorm_1.1-0       infotheo_1.2.0      DBI_1.1.0          
 [49] GGally_2.0.0        Rcpp_1.0.4.6        plotrix_3.7-8      
 [52] viridisLite_0.3.0   progress_1.2.2      foreign_0.8-79     
 [55] stats4_4.0.0        glmnet_3.0-2        httr_1.4.1         
 [58] getopt_1.20.3       RColorBrewer_1.1-2  modeltools_0.2-23  
 [61] ellipsis_0.3.0      R.methodsS3_1.8.0   pkgconfig_2.0.3    
 [64] reshape_0.8.8       CMLS_1.0-0          dbplyr_1.4.3       
 [67] tidyselect_1.1.0    rlang_0.4.8         reshape2_1.4.4     
 [70] PRROC_1.3.1         munsell_0.5.0       cellranger_1.1.0   
 [73] cli_2.0.2           generics_0.0.2      ade4_1.7-15        
 [76] broom_0.5.6         biomformat_1.16.0   fs_1.4.1           
 [79] zip_2.0.4           beanplot_1.2        nlme_3.1-147       
 [82] R.oo_1.23.0         pracma_2.2.9        xml2_1.3.2         
 [85] compiler_4.0.0      rstudioapi_0.11     curl_4.3           
 [88] ggsignif_0.6.0      reprex_0.3.0        stringi_1.4.6      
 [91] multtest_2.44.0     vctrs_0.3.4         pillar_1.4.3       
 [94] lifecycle_0.2.0     LiblineaR_2.10-8    R6_2.4.1           
 [97] gridExtra_2.3       rio_0.5.16          IRanges_2.22.2     
[100] codetools_0.2-16    MASS_7.3-51.6       gtools_3.8.2       
[103] assertthat_0.2.1    MBA_0.0-9           rhdf5_2.32.2       
[106] rprojroot_1.3-2     minpack.lm_1.2-1    withr_2.2.0        
[109] multcomp_1.4-13     S4Vectors_0.26.1    rlist_0.4.6.1      
[112] mgcv_1.8-31         hms_0.5.3           quadprog_1.5-8     
[115] grid_4.0.0          carData_3.0-3       parallelMap_1.5.0  
[118] pROC_1.16.2         Biobase_2.48.0      lubridate_1.7.8    
